package main

import (
	"github.com/BurntSushi/toml"
	"go_nsi/internal/app/api"
	"log"
	"path/filepath"
)

var configPath string

func init() {
	configPath = filepath.Join("configs", "nsi.toml")
}

func main() {
	mainConfig := api.NewConfig()

	_, err := toml.DecodeFile(configPath, mainConfig)
	if err != nil {
		log.Fatal("Не найден .toml файл:", err)
	}
	server := api.NewApi(mainConfig)

	log.Print("starting server")
	log.Fatal(server.StartApi())
}
