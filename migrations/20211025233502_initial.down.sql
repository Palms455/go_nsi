DROP TABLE IF EXISTS  nsi.field;

DROP TABLE IF EXISTS  nsi.field_set;

DROP TABLE IF EXISTS  nsi.refbook_data;

DROP TABLE IF EXISTS  nsi.version;

DROP TABLE IF EXISTS  nsi.refbook;

DROP TABLE IF EXISTS  nsi.source;