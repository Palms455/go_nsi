CREATE SCHEMA IF NOT EXISTS nsi;

CREATE TABLE IF NOT EXISTS  nsi."source" (
    id uuid NOT NULL,
    "name" varchar(250) NULL,
    description text NULL,
    CONSTRAINT source_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS nsi.refbook (
    id uuid NOT NULL,
    "oid" varchar(150) NOT NULL,
    "name" varchar(250) NULL,
    description text NULL,
    source_id uuid NULL,
    CONSTRAINT refbook_pkey PRIMARY KEY (id)
);

ALTER TABLE nsi.refbook ADD CONSTRAINT refbook_source_id_fkey FOREIGN KEY (source_id) REFERENCES nsi.source(id);


CREATE TABLE nsi."version" (
    id uuid NOT NULL,
    version_major int4 NOT NULL,
    version_minor int4 NOT NULL,
    publish_date date NOT NULL,
    description text NOT NULL,
    refbook_id uuid NOT NULL,
    CONSTRAINT version_pkey PRIMARY KEY (id),
    CONSTRAINT version_refbook_id_version_major_version_minor_f066215c_uniq UNIQUE (refbook_id, version_major, version_minor)
);
CREATE INDEX version_refbook_id_ed146760 ON nsi.version USING btree (refbook_id);

-- nsi."version" foreign keys
ALTER TABLE nsi."version" ADD CONSTRAINT version_id_refbook_id FOREIGN KEY (refbook_id) REFERENCES nsi.refbook(id) DEFERRABLE INITIALLY deferred;


CREATE TABLE nsi.field_set (
    id uuid NOT NULL,
    name varchar(250) NOT NULL,
    version_id uuid NOT null,
    CONSTRAINT field_set_pkey PRIMARY KEY (id)
);

ALTER TABLE nsi.field_set
    ADD CONSTRAINT field_set_version_id
        FOREIGN KEY (version_id)
            REFERENCES nsi.version(id)
            DEFERRABLE INITIALLY DEFERRED;


CREATE TABLE nsi.field (
    id uuid NOT NULL,
    name varchar(30) NOT NULL,
    description varchar(100) NOT NULL,
    field_type varchar(50) NOT NULL,
    field_set_id uuid NOT NULL,
    CONSTRAINT field_pkey PRIMARY KEY (id)
);
CREATE INDEX field_field_set_id ON nsi.field USING btree (field_set_id);

-- nsi.field foreign keys
ALTER TABLE nsi.field
        ADD CONSTRAINT field_field_set_id
        FOREIGN KEY (field_set_id)
        REFERENCES nsi.field_set(id)
        DEFERRABLE INITIALLY DEFERRED;

CREATE TABLE  nsi.refbook_data (
    id int NOT NULL,
    ref jsonb NOT NULL,
    version_id uuid NOT NULL
    );

ALTER TABLE nsi.refbook_data
    ADD CONSTRAINT refbook_data_version_id
        FOREIGN KEY (version_id)
            REFERENCES nsi.version(id)
            DEFERRABLE INITIALLY DEFERRED;