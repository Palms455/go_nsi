module go_nsi

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.2
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
