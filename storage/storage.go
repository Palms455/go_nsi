package storage

import (
	"database/sql"
	_ "github.com/lib/pq"
	"log"
)

// Структура для БД
type Storage struct{
	//конфигурация БД
	config *Config
	// БД дескриптор
	db *sql.DB

	*SourceRepository
	*RefbookRepository
	*VersionRepository
	*RefbookDataRepository
}

func NewStorage(config *Config) *Storage {
	return &Storage{
		config: config,
	}
}

//Установление соединения с БД
func (storage *Storage)  Open() error {
	db, err := sql.Open(storage.config.Db_driver, storage.config.ConnString)
	if err != nil {
		return err
	}
	if err := db.Ping(); err != nil {
		return err
	}
	storage.db = db
	log.Println("Соединение с БД установлено")
	return nil
}

//Закрытие соединения с БД
func (storage *Storage) Close() {
	storage.db.Close()
}


func (s *Storage) Source() *SourceRepository {
	if s.SourceRepository != nil {
		return s.SourceRepository
	}
	s.SourceRepository = &SourceRepository{
		storage: s,
	}
	return s.SourceRepository
}

func (s *Storage) Refbook() *RefbookRepository {
	if s.RefbookRepository != nil {
		return s.RefbookRepository
	}
	s.RefbookRepository = &RefbookRepository{
		storage: s,
	}
	return s.RefbookRepository
}

func (s *Storage) Version() *VersionRepository {
	if s.VersionRepository != nil {
		return s.VersionRepository
	}
	s.VersionRepository = &VersionRepository{
		storage: s,
	}
	return s.VersionRepository
}

func (s *Storage) RefbookData() *RefbookDataRepository {
	if s.RefbookDataRepository !=nil {
		return s.RefbookDataRepository
	}
	s.RefbookDataRepository = &RefbookDataRepository{
		storage: s,
	}
	return s.RefbookDataRepository
}