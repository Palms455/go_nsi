package storage

import (
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"go_nsi/internal/app/models"
)

type RefbookDataRepository struct {
	storage *Storage
}

var tableRefbookData = "nsi.refbook_data"


// RefbookValues - Получение данных справочника последней версии
func (data RefbookDataRepository) RefbookValues(oid string) (*models.RefbookData, error) {
	query := fmt.Sprintf(`
	select coalesce(rd.id, 0), rd."ref" , rd.version_id
		from %s rf
		join %s v on v.refbook_id = rf.id
		join %s rd on rd.version_id = v.id
		where rf."oid" = $1
		order by v.version_major desc, v.version_minor desc
		limit 1
	`,
	tableRefbook,
	tableVersion,
	tableRefbookData,
	)
	refbookData := models.RefbookData{}
	err := data.storage.db.QueryRow(query, oid).Scan(&refbookData.Id, &refbookData.Ref, &refbookData.VersionID)
	if err != nil {
		return nil, err
	}
	if refbookData.Id == 0 {
		return nil, sql.ErrNoRows
	}
	return &refbookData, nil
}


// RefbookValuesByVersion - Получение данных справочника указанной версии
func (data RefbookDataRepository) RefbookValuesByVersion(version_id uuid.UUID) (*models.RefbookData, error) {
	query := fmt.Sprintf(`
	select coalesce(rd.id, 0), rd.ref, rd.version_id
		from %s rd
		where rd.version_id = $1
	`,
	tableRefbookData,
	)
	refbookData := models.RefbookData{}
	err := data.storage.db.QueryRow(query, version_id).Scan(&refbookData.Id, &refbookData.Ref, &refbookData.VersionID)
	if err != nil {
		return nil, err
	}
	if refbookData.Id == 0 {
		return nil, sql.ErrNoRows
	}
	return &refbookData, nil
}