package storage


type Config struct {
	ConnString string `toml:"conn_string"`
	Db_driver string `toml:"db_driver"`
}

func NewConfig() *Config {
	return &Config{}
}

