package storage

import (
	"fmt"
	"go_nsi/internal/app/models"
	"log"
)

type VersionRepository struct {
	storage *Storage
}

var tableVersion = "nsi.version"


//Список версий справочника
func (version *VersionRepository) SelectVersionByRefbook(refbook_id string) ([]*models.Version, error) {
	query := fmt.Sprintf("select id, version_major, version_minor, publish_date, refbook_id from %s where refbook_id = $1", tableVersion)
	rows, err := version.storage.db.Query(query, refbook_id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	versionList := make([]*models.Version, 0)
	for rows.Next() {
		version := models.Version{}
		err = rows.Scan(&version.Id, &version.VersionMajor, &version.VersionMinor, &version.PublishDate, &version.RefbookId)
		if err != nil {
			log.Printf("Ошибка при получении списка версии: %s", err)
			continue
		}
		versionList = append(versionList, &version)
	}
	return versionList, nil
}

// ActualVersion - Получение актуальной версии справочника
func (version *VersionRepository) ActualVersion(oid string) (*models.Version, error) {
	query := fmt.Sprintf(`
		select
			v.id, v.version_major, v.version_minor, v.publish_date
		from
			%s rf
			join %s v on v.refbook_id = rf.id
		where
			rf."oid" = $1
		order by v.version_major desc, v.version_minor desc
		limit 1
	`,
	tableRefbook,
	tableVersion,
	)
	v_data := models.Version{}
	err := version.storage.db.QueryRow(query, oid).Scan(&v_data.Id, &v_data.VersionMajor, &v_data.VersionMinor, &v_data.PublishDate)
	fmt.Println(v_data)
	if err != nil {
		return nil, err
	}
	return &v_data, nil
}

// GetVersion - получение версии указанного справочника
func (version *VersionRepository) GetVersion(oid string, v_major, v_minor int) (*models.Version, error) {
	query := fmt.Sprintf(`
		select 
			v.id, v.publish_date
		from 
			%s v
			join %s rf on rf.id = v.refbook_id 
		where 
			rf.oid = $1
			and v.version_major = $2
			and v.version_minor = $3
	`,
	tableVersion,
	tableRefbook,
	)
	v_data := models.Version{
		VersionMajor: v_major,
		VersionMinor: v_minor,
	}
	err := version.storage.db.QueryRow(query, oid, v_major, v_minor).Scan(&v_data.Id, &v_data.PublishDate)
	if err != nil {
		return nil, err
	}
	return &v_data, nil
}

