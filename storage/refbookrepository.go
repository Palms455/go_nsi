package storage

import (
	"fmt"
	"go_nsi/internal/app/models"
	"log"
)

type RefbookRepository struct {
	storage *Storage
}

var (
	tableRefbook = "nsi.refbook"
)

// Список справочников
func (source *RefbookRepository) SelectAll(source_id string) ([]*models.Refbook, error) {
	query := fmt.Sprintf("select id, oid, name, description, source_id from %s", tableRefbook)
	if source_id != "" {
		query += fmt.Sprintf(" where source_id = '%s'", source_id)
			}
	rows, err := source.storage.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	refbookList := make([]*models.Refbook, 0)
	for rows.Next() {
		ref := models.Refbook{}
		err = rows.Scan(&ref.Id, &ref.Oid, &ref.Name, &ref.Description, &ref.SourceId)
		if err != nil {
			log.Printf("Ошибка при получении списка справочников: %v", err)
			continue
		}
		refbookList = append(refbookList, &ref)
	}
	return refbookList, nil

}
