package storage

import (
	"fmt"
	"go_nsi/internal/app/models"
	"log"
)

type SourceRepository struct {
	storage *Storage
}

var (
	tableSource = "nsi.source"
)

//Список источников
func (source *SourceRepository) SelectAll() ([]*models.Source, error) {
	query := fmt.Sprintf("select id, name, description from %s", tableSource)
	rows, err := source.storage.db.Query(query)
	if err != nil {
		return nil ,err
	}
	defer rows.Close()
	sourceList := make([]*models.Source, 0)
	for rows.Next() {
		a := models.Source{}
		err := rows.Scan(&a.Id, &a.Name, &a.Description)
		if err != nil {
			log.Printf("Ошибка при получении списка источников: %s", err)
			continue
		}
		sourceList = append(sourceList, &a)
	}
	return sourceList, nil
}