package api

import "go_nsi/storage"


//Стурктура для конфигурации API
type Config struct {
	// Указываем порт по которому работает сервер
	BindAddr string `toml:"bind_addr"`

	//Уровень логирования
	LogLevel string `toml:"log_level"`

	//Конфигурация БД
	Storage *storage.Config


}

func NewConfig() *Config {
	return &Config{
		BindAddr: ":8080",
		LogLevel: "debug",
		Storage: storage.NewConfig(),
	}

}