package api

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"go_nsi/storage"
	"net/http"
)


var (
	prefix string = "/api/v1"
)

// Конфигурирование логгера
func (api *Api) configLogger() error {
	logLevel, err := logrus.ParseLevel(api.config.LogLevel)
	if err != nil {
		return err
	}
	api.logger.SetLevel(logLevel)
	return nil
}


//КОнфигурирование и запуск соединения с бд
func (api *Api) configStorage() error {
	storageDb := storage.NewStorage(api.config.Storage)
	if err := storageDb.Open(); err != nil {
		return err
	}
	api.storage = storageDb
	return nil
}

func (api *Api) configRouter() {
	api.router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello! This is rest api!"))
	})
	api.router.HandleFunc(prefix+ "/sources/", api.GetAllSource).Methods("GET")
	api.router.HandleFunc(prefix+ "/refbooks/", api.GetAllRefbook).Methods("GET")
	api.router.HandleFunc(prefix+ "/versions/{id}/", api.RefbookVersions).Methods("GET")
	api.router.HandleFunc(prefix+ "/refbook/{oid}/{v_major}.{v_minor}/", api.RefbookData).Methods("GET")
	api.router.HandleFunc(prefix+ "/refbook/{oid}/", api.RefbookData).Methods("GET")
}

func Return500(writer http.ResponseWriter) {
	msg := Message{
		StatusCode: 501,
		Message:    "Произошла ошибка сервиса. Попробуйте позднее",
		IsError:    true,
	}
	writer.WriteHeader(501)
	json.NewEncoder(writer).Encode(msg)
	return
}

func Return400(writer http.ResponseWriter, err string) {
	msg := Message{
		StatusCode: 400 ,
		Message:    err,
		IsError:    true,
	}
	writer.WriteHeader(400)
	json.NewEncoder(writer).Encode(msg)
	return
}



// IsValidUUID проверка на привильность переданного uuid
func IsValidUUID(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}