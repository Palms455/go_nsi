package api

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"go_nsi/storage"
	"net/http"
)

// Базовая структура для API NSI
type Api struct {
	config *Config
	router *mux.Router
	logger *logrus.Logger
	storage *storage.Storage
}

func NewApi(config *Config) *Api {
	return &Api{
		config: config,
		router: mux.NewRouter(),
		logger: logrus.New(),
	}
}

func (api *Api) StartApi () error {
	fmt.Println(api.config)
	if err := api.configLogger(); err != nil {
		return err
	}
	api.logger.Info("Сконифгурировано логгирование ")

	if err := api.configStorage(); err != nil {
		return err
	}
	api.logger.Info("Сконфигурирован доступ к БД")

	api.configRouter()

	api.logger.Info("Запуск сервера")
	return http.ListenAndServe(api.config.BindAddr, api.router)
}
