package api

import (
	"database/sql"
	"encoding/json"
	"github.com/gorilla/mux"
	"go_nsi/internal/app/models"
	"net/http"
	"strconv"
)

//Вспомогательная структура для формирования сообщений
type Message struct {
	StatusCode int    `json:"status_code"`
	Message    string `json:"message"`
	IsError    bool   `json:"is_error"`
}

func initHeaders(writer http.ResponseWriter) {
	writer.Header().Set("Content-Type", "application/json")
}

// GetAllSource - список источников
func (api *Api) GetAllSource(writer http.ResponseWriter, req *http.Request) {
	initHeaders(writer)
	api.logger.Info("Получение списка источников")

	sources, err := api.storage.Source().SelectAll()
	if err != nil {
		//Что делаем, если была ошибка на этапе подключения?
		api.logger.Info("Ошибка при доступе к nsi.source : ", err)
		Return500(writer)
		return
	}
	writer.WriteHeader(200)
	json.NewEncoder(writer).Encode(sources)
}

// GetAllRefbook - список справочников
func (api *Api) GetAllRefbook (writer http.ResponseWriter, req *http.Request)  {
	urlParams := req.URL.Query()
	source_id := urlParams.Get("source")
	if source_id != "" && !IsValidUUID(source_id) {
		Return400(writer, "Указан неправильный идентификатор источника справочника")
		return
	}
	initHeaders(writer)
	api.logger.Info("Получение списка справочников")
	refbooks, err := api.storage.Refbook().SelectAll(source_id)
	if err != nil {
		api.logger.Info("Ошибка доступа к таблице refbook: ", err)
		Return500(writer)
		return
	}
	writer.WriteHeader(200)
	json.NewEncoder(writer).Encode(refbooks)
}

// GetRefbookVersions
func (api *Api) RefbookVersions (writer http.ResponseWriter, req *http.Request) {
	initHeaders(writer)
	api.logger.Info("Получение версии")
	refbook_id := mux.Vars(req)["id"]
	if !IsValidUUID(refbook_id) {
		api.logger.Warning("Указан не валидный uuid")
		Return500(writer)
		return
	}
	versions, err := api.storage.Version().SelectVersionByRefbook(refbook_id)
	if err != nil {
		api.logger.Warning("Ошибка при получении списка версии: ", err)
		Return500(writer)
		return
	}
	writer.WriteHeader(200)
	json.NewEncoder(writer).Encode(versions)
}


// RefbookData - получение данных справочника
func (api *Api) RefbookData(writer http.ResponseWriter, req *http.Request) {
	initHeaders(writer)
	api.logger.Info("Получение данных справочника")
	v_major, major_ok := mux.Vars(req)["v_major"]
	v_minor, minor_ok := mux.Vars(req)["v_minor"]
	refbook_oid := mux.Vars(req)["oid"]
	var Version *models.Version
	var err error
	if major_ok && minor_ok {
		version_major, err := strconv.Atoi(v_major)
		if err != nil {
			Return400(writer, "Мажорная версия должна быть числом")
			api.logger.Info(err)
			return
		}
		version_minor, err := strconv.Atoi(v_minor)
		if err != nil {
			Return400(writer, "Минорная версия должна быть числом")
			api.logger.Info(err)
			return
		}
		Version, err = api.storage.Version().GetVersion(refbook_oid, version_major, version_minor)
		if err != nil {
			if err == sql.ErrNoRows {
				writer.WriteHeader(404)
				return
			} else {
				Return500(writer)
				api.logger.Info(err)
				return
			}
		}
	} else {
		Version, err = api.storage.Version().ActualVersion(refbook_oid)
		if err != nil {
			if err == sql.ErrNoRows {
				writer.WriteHeader(404)
				return
			} else {
				Return500(writer)
				api.logger.Info(err)
				return
			}
		}
	}

	data, err := api.storage.RefbookData().RefbookValuesByVersion(Version.Id)
	if err == sql.ErrNoRows {
		writer.WriteHeader(404)
		return
	}
	if err != nil {
		api.logger.Info("Ошибка при получении знaчений справочника: ", err)
		Return500(writer)
	}
	writer.WriteHeader(200)
	json.NewEncoder(writer).Encode(data)
}
