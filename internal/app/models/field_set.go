package models

import "github.com/google/uuid"

//FieldSet - набор полей
type FieldSet struct {
	Id uuid.UUID `json:"id"`
	Name string `json:"name"`
}
