package models

import "github.com/google/uuid"

//Source - источник справочников
type Source struct {
	Id uuid.UUID `json:"id"`
	Name string `json:"name"`
	Description NullString `json:"description"`
}
