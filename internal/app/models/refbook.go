package models

import "github.com/google/uuid"

//Refbook -справочник
type Refbook struct {
	Id uuid.UUID `json:"id"`
	Oid string `json:"oid"`
	Name NullString `json:"name"`
	Description NullString `json:"description"`
	SourceId uuid.UUID `json:"source_id"`
	Version string `json:"version,omitempty"`
}


