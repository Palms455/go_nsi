package models

import "github.com/google/uuid"

//Field - поле справочника
type Field struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Description NullString `json:"description"`
	FieldType string `json:"field_type"`
	FieldSetId uuid.UUID `json:"field_set_id"`
}


