package models

import "github.com/google/uuid"

//RefbookData - данные справочника
type RefbookData struct {
	Id int `json:"id"`
	Ref Jsonb `json:"ref"`
	VersionID uuid.UUID `json:"version_id"`
}