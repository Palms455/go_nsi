package models

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"github.com/google/uuid"
	"reflect"
)

// NullString - переопределение типа String при nil значениях
type NullString sql.NullString


func (n *NullString) Scan(value interface{}) error {
	var s sql.NullString
	if err := s.Scan(value); err != nil {
		return err
	}
	if reflect.TypeOf(value) == nil {
		*n = NullString{s.String, false}
	} else {
		*n = NullString{s.String, true}
	}
	return nil

}

func (n *NullString) MarshalJSON() ([]byte, error) {
	if !n.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(n.String)
}


func (n *NullString) UnmarshalJSON(b []byte) error {
	err := json.Unmarshal(b, &n.String)
	n.Valid = err == nil
	return err
}


type nUUID uuid.UUID

func (u nUUID) MarshalText() ([]byte, error) {
	if u == nUUID(uuid.Nil) {
		return nil, nil
	}
	return uuid.UUID(u).MarshalText()
}

func (u nUUID) String() string {
	return uuid.UUID(u).String()
}

func (u *nUUID) Scan(value interface{}) error {
	var uid uuid.UUID
	if err := uid.Scan(value); err != nil {
		return err
	}
	*u = nUUID(uid)
	return nil
}

type Jsonb []map[string]interface{}

func (a Jsonb) Value() (driver.Value, error) {
	return json.Marshal(a)
}

func (a *Jsonb) Scan(value interface{}) error {
	b, ok := value.([]byte)
	if !ok {
		return nil
	}
	return json.Unmarshal(b, &a)
}