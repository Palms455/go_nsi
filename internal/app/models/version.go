package models

import (
	"github.com/google/uuid"
	"time"
)



//Version -Версия справочника
type Version struct {
	Id uuid.UUID `json:"id"`
	VersionMajor int `json:"version_major"`
	VersionMinor int `json:"version_minor"`
	PublishDate time.Time `json:"publish_date"`
	RefbookId nUUID `json:"refbook_id,omitempty"`
}

